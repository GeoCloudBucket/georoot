""" This App calculates mortgage monthly payments, and total payment of the loan."""

import math
import tkinter
import tkinter.ttk as ttk


def calc_mortgage():
    # Function that calculates the mortgage numbers.
    try:
        # Configure progressbar
        s.configure("red.Horizontal.TProgressbar", foreground="red", background="blue")
        progressbar = ttk.Progressbar(master, style="red.Horizontal.TProgressbar", maximum=100, mode="determinate")
        progressbar.grid(row=8, column=0, columnspan=5, pady=5, padx=0)

        # Update progressbar
        current_v = 90
        progressbar.after(500, progress(progressbar, current_v))
        progressbar.update()

        # Calculations

        principal, interest, years = int(e1.get()), float(e2.get()), int(e3.get())

        interest_rate = interest / (100 * 12)
        payment_num = years * 12
        payment = principal * (interest_rate / (1 - math.pow((1 + interest_rate), (-payment_num))))

        total_amount = payment * years * 12

        # Labels to show the calculation results.

        monthly_value = tkinter.Label(master, text="{:,.2f}".format(payment), font=("Helvetica", 10))
        monthly_value.grid(sticky="W", row=6, column=1, pady=1, padx=5)

        total_value = tkinter.Label(master, text="{:,.2f}".format(total_amount), font=("Helvetica", 10))
        total_value.grid(sticky="W", row=7, column=1, pady=1, padx=5)

        # Update progressbar, and set the color to green to indicate successful run.
        s.configure("red.Horizontal.TProgressbar", foreground="red", background="green")
        current_v = 100
        progressbar.after(500, progress(progressbar, current_v))
        progressbar.update()

    except Exception as e:
        # Update progressbar and set the color to red, to indicate an issue.
        s.configure("red.Horizontal.TProgressbar", foreground="red", background="red")
        current_v = 90
        progressbar.after(500, progress(progressbar, current_v))
        progressbar.update()
        print(e)


def progress(progressbar, current):
    # This function updates the progressbar.
    progressbar["value"] = current


if __name__ == "__main__":

    try:
        # Sets the main GUI window for the app
        master = tkinter.Tk()
        master.title('mCalc')
        master.geometry("270x230+500+300")

        s = ttk.Style()
        s.theme_use("classic")

        # The Labels to be used.

        principalLabel = tkinter.Label(master, text='Principal', font=("Helvetica", 14))
        principalLabel.grid(sticky="W", row=0, column=0, pady=0, padx=10)

        interestLabel = tkinter.Label(master, text='Interest', font=("Helvetica", 14))
        interestLabel.grid(sticky="W", row=1, column=0, pady=0, padx=10)

        yearsLabel = tkinter.Label(master, text='Years', font=("Helvetica", 14))
        yearsLabel.grid(sticky="W", row=2, column=0, pady=0, padx=10)

        monthlyLabel = tkinter.Label(master, text='Monthly:', font=("Helvetica", 14))
        monthlyLabel.grid(sticky="W", row=6, column=0, pady=0, padx=10)

        totalLabel = tkinter.Label(master, text='Total:', font=("Helvetica", 14))
        totalLabel.grid(sticky="W", row=7, column=0, pady=0, padx=10)

        # The user input boxes to be used.

        e1 = tkinter.Entry(master, font=("Helvetica", 10))
        e1.grid(sticky="W", row=0, column=1, pady=0, padx=10)

        e2 = tkinter.Entry(master, font=("Helvetica", 10))
        e2.grid(sticky="W", row=1, column=1, pady=0, padx=10)

        e3 = tkinter.Entry(master, font=("Helvetica", 10))
        e3.grid(sticky="W", row=2, column=1, pady=0, padx=10)

        e1.insert(0, "300000")
        e2.insert(0, "4.5")
        e3.insert(0, "30")

        # Separator line for aesthetics.
        ttk.Separator(master, orient=tkinter.HORIZONTAL).grid(row=3, columnspan=2, pady=2, sticky=tkinter.EW)

        # Button that calls the calc_mortgage function to calculate the mortgage numbers.
        button = tkinter.Button(master, text='Calculate', font=("Helvetica", 14), command=calc_mortgage)
        button.grid(row=4, column=0, columnspan=2, pady=2, padx=5)

        # Separator line for aesthetics.
        ttk.Separator(master, orient=tkinter.HORIZONTAL).grid(row=5, columnspan=2, pady=2, sticky=tkinter.EW)

        master.mainloop()

    except Exception as E:
        print(E)
