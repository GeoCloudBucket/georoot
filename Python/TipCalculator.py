""" This App calculates the Tip of a given bill amount. """

import tkinter
import tkinter.ttk as ttk


def calc_tip():

    try:
        # Configure progressbar
        s.configure("red.Horizontal.TProgressbar", foreground="red", background="blue")
        progressbar = ttk.Progressbar(master, style="red.Horizontal.TProgressbar", maximum=100, mode="determinate")
        progressbar.grid(row=8, column=0, columnspan=5, pady=5, padx=0)

        # Update progressbar
        current_v = 90
        progressbar.after(500, progress(progressbar, current_v))
        progressbar.update()

        # Calculations

        bill = float(e1.get())
        radial = var.get()

        if radial == 1:
            percentage = 15
        elif radial == 2:
            percentage = 18
        elif radial == 3:
            percentage = 20

        tip = percentage * 0.01 * bill
        total = bill + tip

        # Labels to show the calculation results.

        monthly_value = tkinter.Label(master, text="$ " + "{:,.2f}".format(tip), font=("Helvetica", 10))
        monthly_value.grid(sticky="W", row=6, column=1, pady=1, padx=5)

        total_value = tkinter.Label(master, text="$ " + "{:,.2f}".format(total), font=("Helvetica", 10))
        total_value.grid(sticky="W", row=7, column=1, pady=1, padx=5)

        # Update progressbar, and set the color to green to indicate successful run.
        s.configure("red.Horizontal.TProgressbar", foreground="red", background="green")
        current_v = 100
        progressbar.after(500, progress(progressbar, current_v))
        progressbar.update()

    except Exception as e:
        print(e)
        # Update progressbar and set the color to red, to indicate an issue.
        s.configure("red.Horizontal.TProgressbar", foreground="red", background="red")
        current_v = 90
        progressbar.after(500, progress(progressbar, current_v))
        progressbar.update()


def progress(progressbar, current):
    # This function updates the progressbar.
    progressbar["value"] = current


if __name__ == "__main__":

    try:
        # Sets the main GUI window for the app
        master = tkinter.Tk()
        master.title('iTip')
        master.geometry("250x200+500+300")

        s = ttk.Style()
        s.theme_use("classic")

        # The Labels to be used.

        billLabel = tkinter.Label(master, text='Bill', font=("Helvetica", 14))
        billLabel.grid(sticky="W", row=0, column=0, pady=0, padx=10)

        percentLabel = tkinter.Label(master, text='Tip %', font=("Helvetica", 14))
        percentLabel.grid(sticky="W", row=1, column=0, pady=0, padx=10)

        monthlyLabel = tkinter.Label(master, text='Tip:', font=("Helvetica", 14))
        monthlyLabel.grid(sticky="W", row=6, column=0, pady=0, padx=10)

        totalLabel = tkinter.Label(master, text='Total:', font=("Helvetica", 14))
        totalLabel.grid(sticky="W", row=7, column=0, pady=0, padx=10)

        # The user input boxes to be used.

        e1 = tkinter.Entry(master, font=("Helvetica", 10))
        e1.grid(sticky="W", row=0, column=1, columnspan=3, pady=0, padx=10)
        e1.insert(0, "100.00")

        var = tkinter.IntVar(value=1)
        r1 = tkinter.Radiobutton(master, text="15%", variable=var, value=1, font=("Helvetica", 10))
        r1.grid(sticky="W", row=1, column=1, pady=0, padx=0)

        r2 = tkinter.Radiobutton(master, text="18%", variable=var, value=2, font=("Helvetica", 10))
        r2.grid(sticky="W", row=1, column=2, pady=0, padx=0)

        r3 = tkinter.Radiobutton(master, text="20%", variable=var, value=3, font=("Helvetica", 10))
        r3.grid(sticky="W", row=1, column=3, pady=0, padx=0)

        # Separator line for aesthetics.
        ttk.Separator(master, orient=tkinter.HORIZONTAL).grid(row=3, columnspan=4, pady=2, sticky=tkinter.EW)

        # Button that calls the calc_tip function to calculate the tip and total numbers.
        button = tkinter.Button(master, text='Calculate', font=("Helvetica", 14), command=calc_tip)
        button.grid(row=4, column=0, columnspan=4, pady=1, padx=5)

        # Separator line for aesthetics.
        ttk.Separator(master, orient=tkinter.HORIZONTAL).grid(row=5, columnspan=4, pady=2, sticky=tkinter.EW)

        master.mainloop()

    except Exception as E:
        print(E)
