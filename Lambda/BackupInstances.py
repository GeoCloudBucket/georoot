""" This Lambda searches for EC2s with the specified filtered tags,
backs up the instance(s) volumes. It takes a daily snapshot everyday,
and a monthly snapshot on the first of each month. It also deletes them
based on specified timeframe."""

import json
import time
import boto3
import datetime


def lambda_handler(event, context):
    try:
        client = boto3.client("ec2")

        # Date formatting
        datetime_format = '%Y-%m-%d %H:%M:%S'
        timetmp = datetime.datetime.now()
        Time = timetmp.strftime('%b-%d-%Y_%H.%M.%S')
        todaydate = datetime.date.today()

        instanceID, volumeID, name = [], [], []

        ec2response = get_instances(client)
        print(ec2response)

        for ec2 in ec2response["Reservations"]:
            instanceID = ec2["Instances"][0]["InstanceId"]
            print(instanceID)

            for vol in ec2['Instances'][0]['BlockDeviceMappings']:
                volumeID = vol['Ebs']['VolumeId']
                print(volumeID)

                for tag in ec2['Instances'][0]['Tags']:
                    if tag['Key'] == 'Name':
                        name = tag['Value']
                        print(name)

                print(todaydate.day)

                if todaydate.day == 1 or 8 or 15 or 22:
                    create_daily_snapshots(client, instanceID, Time, name, volumeID)
                    time.sleep(20)

                if todaydate.day == 1:
                    create_monthly_snapshots(client, instanceID, Time, name, volumeID)
                    time.sleep(20)

        oldSnapShotsDaily, oldSnapShotsMonthly = get_old_snapshots(client, datetime_format)

        deletedSnashots = delete_old_snapshot(client, oldSnapShotsDaily, oldSnapShotsMonthly)

        print(deletedSnashots)

    except Exception as E:
        print(E)


def get_instances(client):
    try:
        response = client.describe_instances(
            Filters=[{"Name": "tag:AGS", "Values": ["TEST"]},
                     {"Name": "tag:SDLC", "Values": ["DEV"]}
                     ]
        )
    except Exception as e:
        print(e)

    return response


def create_daily_snapshots(client, instanceID, Time, name, volumeID):
    print(volumeID)

    response = json.dumps({})
    description = ("TEST_DEV_DAILY_" + instanceID + "_" + Time)
    tags = [{'Key': 'AGS', 'Value': 'TEST'},
            {'Key': 'SDLC', 'Value': 'DEV'},
            {'Key': 'BACKUP', 'Value': 'DAILY_BACKUP'}]

    try:
        response = client.create_snapshot(
            Description=description,
            VolumeId=volumeID,
            TagSpecifications=[{'ResourceType': 'snapshot', 'Tags': [{'Key': 'Name', 'Value': name}] + tags}])

        print(response)

    except Exception as e:
        print(e)


def create_monthly_snapshots(client, instanceID, Time, name, volumeID):
    print(volumeID)

    response = json.dumps({})
    description = ("TEST_DEV_MONTHLY_" + instanceID + "_" + Time)
    tags = [{'Key': 'AGS', 'Value': 'TEST'},
            {'Key': 'SDLC', 'Value': 'DEV'},
            {'Key': 'BACKUP', 'Value': 'MONTHLY_BACKUP'}]

    try:
        response = client.create_snapshot(
            Description=description,
            VolumeId=volumeID,
            TagSpecifications=[{'ResourceType': 'snapshot', 'Tags': [{'Key': 'Name', 'Value': name}] + tags}])

        print(response)

    except Exception as e:
        print(e)


def get_old_snapshots(client, datetime_format):

    timeframeDaily = datetime.datetime.now() - datetime.timedelta(days=30)
    timeframeMonthly = datetime.datetime.now() - datetime.timedelta(days=65)

    timeNowDaily = timeframeDaily.strftime(datetime_format)
    timeNowMonthly = timeframeMonthly.strftime(datetime_format)

    epochDaily = int(time.mktime(time.strptime(timeNowDaily, datetime_format)))
    epochMonthly = int(time.mktime(time.strptime(timeNowMonthly, datetime_format)))

    filterDaily = [{'Name': 'tag:AGS', 'Values': ['TEST']},
                   {'Name': 'tag:SDLC', 'Values': ['DEV']},
                   {'Name': 'tag:BACKUP', 'Values': ['DAILY_BACKUP']}]

    filterMonthly = [{'Name': 'tag:AGS', 'Values': ['TEST']},
                     {'Name': 'tag:SDLC', 'Values': ['DEV']},
                     {'Name': 'tag:BACKUP', 'Values': ['MONTHLY_BACKUP']}]

    responseDaily = client.describe_snapshots(Filters=filterDaily)['Snapshots']
    responseMonthly = client.describe_snapshots(Filters=filterMonthly)['Snapshots']

    oldSnapShotsDaily, oldSnapShotsMonthly = [], []

    try:
        for snapshot in responseDaily:
            timeCreated = snapshot['StartTime']
            timeCreatedFormatted = timeCreated.strftime(datetime_format)
            oldSnapShotsDailyID = snapshot['SnapshotId']
            epochCreatedDaily = int(time.mktime(time.strptime(timeCreatedFormatted, datetime_format)))
            if epochCreatedDaily < epochDaily:
                oldSnapShotsDaily.append(oldSnapShotsDailyID)

        for snapshot in responseMonthly:
            timeCreated = snapshot['StartTime']
            timeCreatedFormatted = timeCreated.strftime(datetime_format)
            oldSnapShotsMonthlyID = snapshot['SnapshotId']
            epochCreatedMonthly = int(time.mktime(time.strptime(timeCreatedFormatted, datetime_format)))
            if epochCreatedMonthly < epochMonthly:
                oldSnapShotsMonthly.append(oldSnapShotsMonthlyID)

    except Exception as e:
        print(e)

    print(oldSnapShotsDaily)
    print(oldSnapShotsMonthly)

    return oldSnapShotsDaily, oldSnapShotsMonthly


def delete_old_snapshot(client, oldSnapShotsDaily, oldSnapShotsMonthly):
    oldSnapShots = oldSnapShotsDaily + oldSnapShotsMonthly

    print(oldSnapShots)

    try:
        for id in oldSnapShots:
            response = client.delete_snapshot(SnapshotId=id)
            time.sleep(20)
    except Exception as e:
        print(e)

    return response

