""" This Lambda searches for EC2s with the specified filtered tags,
stops or starts the instance(s) depending on the time the cloud watch schedule runs.
For instance, if it runs between 22 and 0 UTC, the instances found are stopped,
but if it runs between 12 and 14 UTC, the instances found are started."""

import boto3
import datetime


def lambda_handler(event, context):
    try:
        client = boto3.client("ec2")
        # Fetch the current time (UTC), and format to hour only.
        timetmp = datetime.datetime.now()
        Time = timetmp.strftime("%H")
        Time = int(Time)

        print(Time)

        ec2response = get_instances(client)
        print(ec2response)

        # Extract the Instance ID from the payload.
        for ec2 in ec2response["Reservations"]:
            instanceID = ec2["Instances"][0]["InstanceId"]
            print(instanceID)

        # If time more than equals 22 and less than 0, stop instance(s)
        if Time >= 22 and Time <= 0:
            stoppedInstances = stop_intances(client, instanceID)
        # If time more than equals 12 and less than 14, start instance(s)
        elif Time >= 12 and Time <= 14:
            startedInstances = start_instances(client, instanceID)

    except Exception as E:
        print(E)


def get_instances(client):
    # Make call to fetch instances.
    try:
        response = client.describe_instances(
            Filters=[{"Name": "tag:AGS", "Values": ["TEST"]},
                     {"Name": "tag:SDLC", "Values": ["DEV"]}
                     ]
        )
    except Exception as e:
        print(e)

    return response


def stop_intances(client, instanceID):
    # MAke call to stop instances.
    try:
        response = client.stop_instances(InstanceIds=[instanceID])
        stoppedInstances = response

    except Exception as e:
        print(e)

    print(stoppedInstances)

    return stoppedInstances


def start_instances(client, instanceID):
    # Make call to start instances.
    try:
        response = client.start_instances(InstanceIds=[instanceID])
        startedInstances = response

    except Exception as e:
        print(e)

    print(startedInstances)

    return startedInstances
